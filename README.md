# Team 702 Pixy and LIDARLite Code and Examples#

The purpose of these libraries are to provide working Java libraries for vision tracking and distance measurement for FRC teams. This repository contains code for both the [Pixy CMUcam5](http://cmucam.org/projects/cmucam5) and [LIDARLite (V1 and V2)](https://github.com/PulsedLight3D) sensors. 

## Pixy Camera ##

The Pixy is an inexpensive camera that performs on-board object detection and makes the data available over I2C, SPI, or RS-232. The Pixy is trained using an application called PixyMon where the user selects color signatures for the camera to track. The benefit of using the Pixy over a solution like OpenCV with a webcam is that the Pixy can detect objects at 50Hz.

The code provided in the libraries implements a class to read this data over either the RoboRIO's Main or MXP I2C bus with a default Pixy address of 0x54. The class also provides a variety of methods to perform common tasks and calculations on the detected objects in order to determine angle from center, distance from object(s), or relative horizontal offset. These can then be used to implement automatic drive functions on the robot to turn, strafe, or drive towards a detected object. Multiple instances of the Pixy class can be instantiated to use more than one camera at a time.

Examples are provided in the repository.

## LIDARLite ##

The LIDARLite uses an eye-safe infrared laser to measure distance with 1 inch accuracy up to 45 feet and can perform measurements at up to 500Hz. The distance measurements from the Lidar are read over the I2C bus.

The LIDARLite class in the repository creates a thread on the RoboRio to periodically read distance from the Lidar to use in automated driving. While the Lidar is very accurate on non-reflective surfaces, it is unreliable or unusable on reflective or transparent material such as polished metal or acrylic. Orienting the Lidar at a slight angle towards the ground helps make it slightly more reliable; however, you should always test it on the material you intend to use.

A new method of using the LIDARLite is running the LIDAR over the DIO (Digital Input) section of the roboRIO. This new method allows the LIDAR and the Pixy to run simultaneously and helps to prevent bus crashes that occurred when running both at the same time.

## Motivations For Project ##

We hope to provide an open source and fully functioning library that allows all FRC teams to take advantage of accurate vision tracking. We, at Team 702, are dedicated to the idea that all FRC teams should have an equal opportunity to compete on a level playing field, regardless of the team's resources or tenure.