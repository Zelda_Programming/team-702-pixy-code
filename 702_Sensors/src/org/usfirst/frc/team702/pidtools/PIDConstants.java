package org.usfirst.frc.team702.pidtools;

/**
 * @author sbaron
 * @co-author ereese
 *
 */
public class PIDConstants {

	public double kP = 0;
	public double kI = 0;
	public double kD = 0;
	public double kF = 0;
	
	public PIDConstants(double p, double i, double d, double f)
	{
		kP = p;
		kI = i;
		kD = d;
		kF = f;
	}
	
}
