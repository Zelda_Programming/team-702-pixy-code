package org.usfirst.frc.team702.pidtools;

import edu.wpi.first.wpilibj.PIDOutput;

/**
 * @author sbaron
 * @co-author ereese
 *
 */
public class PIDOutputTarget implements PIDOutput {
	
	private double m_output_value = 0;

	@Override
	public void pidWrite(double output) {
		setOutput(output);
	}

	public synchronized double getOutput() {
		return m_output_value;
	}

	protected synchronized void setOutput(double m_output_value) {
		this.m_output_value = m_output_value;
	}

}
